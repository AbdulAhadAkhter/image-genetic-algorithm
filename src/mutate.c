#include "a4.h"
#include <stdio.h>
#include <stdlib.h>

void mutate(Individual *individual, double rate) {
  Individual *img = individual;
  int n = img->image.width * img->image.height;
  // printf("herpaderpaderp: %d ", n);
  int radius = (int)rate / 100 * n;
  // For a random selection of pixels, change the color data
  for (int i = 0; i < radius; i++) {
    // Pick a random pixel
    int j = rand() % (100 * n);
    img[j].image.data->r = rand() % 256;
    img[j].image.data->g = rand() % 256;
    img[j].image.data->b = rand() % 256;
  }
}
void mutate_population(Individual *individual, int population_size,
                       double rate) {

  int startingIndex = population_size / 4;
  for (int i = startingIndex; i < population_size; i++) {
    // mutate each individual in population
    mutate(&individual[i], rate);
  }
}
