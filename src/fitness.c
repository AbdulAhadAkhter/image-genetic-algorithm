#include "a4.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// double dist(const PIXEL *A, const PIXEL *B) {
//   return (double)((A[i].r - B[i].r) * (A->r - B->r) +
//                   (A->g - B->g) * (A->g - B->g) +
//                   (A->b - B->b) * (A->b - B->b));
// }

double comp_distance(const PIXEL *A, const PIXEL *B, int image_size) {
  int sum = 0;
  int f = 0;
  double squareroot;
  for (int i = 0; i < image_size; i++) {
    // Calculate distance between both images for each index
    sum = ((A[i].r - B[i].r) * (A[i].r - B[i].r) +
           (A[i].g - B[i].g) * (A[i].g - B[i].g) +
           (A[i].b - B[i].b) * (A[i].b - B[i].b));
    // Sum up distance for all of them
    f += sum;
    // printf("value %d is %d %d %d %d %d %d %d: \n", i, A[i].r, B[i].r, A[i].g,
    //        B[i].g, A[i].b, B[i].b, f);
    // printf("sum %d f %d\n", sum, f);
  }
  // Calculate square root converting int to double
  squareroot = sqrt((double)f);
  printf("sqrt %f\n", squareroot);
  return squareroot;
}

void comp_fitness_population(const PIXEL *image, Individual *individual,
                             int population_size) {
  Individual *img = individual;
  int height = img->image.height;
  int width = img->image.width;
  int size = height * width;
  // printf("%d\n", size);
  for (int i = 0; i < population_size; i++) {
    individual[i].fitness =
        comp_distance(image, individual[i].image.data, size);
    // printf("fitness is:%d\n", individual[i].fitness);
    // printf("outputs are derping\n");
  }
  printf("finessing game\n");
};
