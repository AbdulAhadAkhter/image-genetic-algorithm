#include "a4.h"
#include <stdio.h>
#include <stdlib.h>
//
int cmpfunc(const void *a, const void *b) { return (*(int *)a - *(int *)b); }

PPM_IMAGE *evolve_image(const PPM_IMAGE *image, int num_generations,
                        int population_size, double rate) {
  printf("derp: %d %d %d\n", (image->width), (image->height), 255);

  // initialize population of size population_size
  Individual *population =
      (Individual *)malloc(population_size * sizeof(Individual));

  // Generate population image in the individual population
  printf("Gen image\n");
  population = generate_population(population_size, (image->width),
                                   (image->height), 255);

  // Calculate fitness of that population
  printf("Fitness\n");
  for (int j = 0; j < population_size; j++) {
    printf("\n rgb values %d %d %d \n", image[j].data->r, image[j].data->g,
           image[j].data->b);
    comp_fitness_population(image[j].data, population + j,
                            (image->width) * (image->height));
  }
  //
  printf("Computed fitness of pop\n");
  // Sort the population in non-decreasing order
  qsort(population, population_size, sizeof(Individual), cmpfunc);
  // for (int k = 0; k < num_generations; k++) {
  //   // PPM_IMAGE derp = crossover(population, population_size);
  //
  //   int mutation = population_size / 4;
  //   printf("%d", mutation);
  // }

  return 0;
  // image->width = width;
  // image->height = height;
  // size = width*height*3
  //
};

void free_image(PPM_IMAGE *p) {
  free(p->data);
  free(p);
};
